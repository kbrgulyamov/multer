const mongoose = require('mongoose')

const Users = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    avatar: {
        type: String,
        default: "https://toppng.com/uploads/preview/roger-berry-avatar-placeholder-11562991561rbrfzlng6h.png"
    },
})

module.exports = mongoose.model("Users", Users) 