// Импортирует наш фреймворк
const express = require("express")

// Создаем главную переменннею
const app = express()

const bodyParser = require("body-parser");
const cors = require('cors')
const mongoose = require('mongoose')
const PORT = process.env.PORT || 7777

app.use(cors())
app.use(bodyParser.json())

// Routes
app.use("/users", require("./routes/users.js"))

app.get('/', (req, res) => {
    res.send("<h1 style='font-family: Gilroy;'>nothing</h1>")
})

if (process.env.NODE_ENV == "production") {
    console.log("production");

    // Global connect mondoDB
    mongoose.connect(`mongodb+srv://${process.env.MONGO_LOGIN}:${process.env.MONGO_PASSWORD}@seveneleven.nr55j.mongodb.net/?retryWrites=true&w=majority`, () => {
        console.log("ПОДКЛЮЧИЛИСЬ К PRODUCTION DB :)");
    })
    
    mongoose.set("debug", true)
} else {
    console.log("development")
    
    mongoose.connect("mongodb://localhost:27017/app", () => {
        console.log("ПОДКЛЮЧИЛИСЬ К DEVELOPMENT DB :)");
    })

    mongoose.set("debug", true)
}

// Слушатель по порту
app.listen(PORT, () => {
    console.log(`Сервер слушает порт: ${PORT}`);
})

// Можно перейти по домену localhost:8800