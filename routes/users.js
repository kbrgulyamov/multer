const express = require("express")
const router = express.Router()
const Users = require("../models/users")
// Хеширование паролей
const bcrypt = require('bcryptjs')
const salt = bcrypt.genSaltSync(10);
// Файлы
const sharp  = require('sharp')
const multer  = require('multer')
const path = require('path')
const fs = require('fs')
const storage = new multer.diskStorage({
    destination : path.resolve(__dirname, ".","./uploads"),
    filename : function(req, file, callback) {
        callback(null, file.originalname)
    }
})
const upload = multer(storage)

router.post("/register", async (req, res) => {
    Users.create(req.body, (err, data) => {
        if(data) {
            res.json({
                ok: true,
                body: data
            })
        }
    })
})

// JSON-запрос
router.patch("/update/:id", async (req, res) => {
    try {
        Users.findByIdAndUpdate(req.params.id, req.body, (error, data) => {
            if(data) {
                res.json({
                    message: "User updateD!",
                    ok: true
                })
            }
        })
    } catch (error) {
        console.log(error);
    }
})

// FormData-запрос
router.patch("/avatar/:id", upload.single("file"), async (req, res) => {
    const { filename: image } = req.file 

    await sharp(req.file.path)
     .resize(500)
     .jpeg({quality: 50})
     .toFile(
         path.resolve(req.file.destination,'resized',image)
     )
     fs.unlinkSync(req.file.path)
 
     return res.send('SUCCESS!')
})

module.exports = router